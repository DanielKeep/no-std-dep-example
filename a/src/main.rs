#![cfg_attr(not(feature="std"), feature(lang_items))]
#![cfg_attr(not(feature="std"), feature(start))]
#![cfg_attr(not(feature="std"), no_std)]

extern crate b;

#[cfg(not(feature="std"))]
extern "C" {
    fn printf(fmt: *const u8, ...) -> i32;
}

#[cfg(not(feature="std"))]
#[start]
fn start(_argc: isize, _argv: *const *const u8) -> isize {
    unsafe {
        printf("forty_two: %d\n\0".as_bytes().as_ptr(), b::forty_two());
    }
    0
}

#[cfg(feature="std")]
fn main() {
    println!("forty_two: {}", b::forty_two());
}

#[cfg(not(feature="std"))]
pub mod runtime {
    // These functions and traits are used by the compiler, but not
    // for a bare-bones hello world. These are normally
    // provided by libstd.
    #[lang = "eh_personality"] extern fn eh_personality() {}
    #[lang = "eh_unwind_resume"] extern fn eh_unwind_resume() {}
    #[lang = "panic_fmt"] fn panic_fmt() -> ! { loop {} }
    #[no_mangle] pub extern fn rust_eh_register_frames () {}
    #[no_mangle] pub extern fn rust_eh_unregister_frames () {}
}
