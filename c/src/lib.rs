#![cfg_attr(not(feature="std"), no_std)]

#[cfg(not(feature="std"))]
pub fn forty_two() -> i32 {
    21
}

#[cfg(feature="std")]
pub fn forty_two() -> i32 {
    let v: f32 = "42".parse().unwrap();
    v as i32
}
