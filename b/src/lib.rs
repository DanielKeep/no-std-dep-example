#![no_std]

extern crate c;

pub fn forty_two() -> i32 {
    c::forty_two()
}
